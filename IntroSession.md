# Biology Track Info Session

Thursday September 2017 16h-18h35.

## 16h - General intro / Q&A with Beatriz, Mario and Nika 

Join us first to get an overview of the Bio track, and discuss any doubts you have about courses / rotations / etc...

https://istaustria.zoom.us/j/99561065887?pwd=RWE5RmtEU01jeWk2bFdBVE5JZlJIQT09

**Meeting ID:** 995 6106 5887
**Passcode:** 552669

## 17h - 18h35 - Meetings with PIs

At your allocated time slot, join PIs in their individual Zoom rooms:

### Leonid Sazanov
https://istaustria.zoom.us/j/98066610904?pwd=UHRYeFpndkVjTGhXTFpsR0JkR2p4dz09

**Meeting ID:** 980 6661 0904
**Passcode:** 513209

***
### Martin Loose
https://istaustria.zoom.us/j/92541952446?pwd=V21LY25qMTJKSEFFTzk1ZStmT1piUT09

**Meeting ID:** 925 4195 2446
**Passcode:** 744482

***
### Florian Schur
https://istaustria.zoom.us/j/92328942030?pwd=NkR2Zkd0WTcyQmhzbmRld3M4RjNwUT09

**Meeting ID:** 923 2894 2030
**Passcode:** 732755

***
### Carrie Bernecky
https://istaustria.zoom.us/j/99841072525?pwd=dXJXYWhMRHdFMi9ReGlLSitvdlVYZz09

**Meeting ID:** 998 4107 2525
**Passcode:** 451900

***
### Matthew Robinson
https://istaustria.zoom.us/j/92435537193?pwd=Y0tDQ3BvYnI0RVdVR1ZnREdaZmVwdz09

**Meeting ID:** 924 3553 7193
**Passcode:** 639695

***
### Johann Danzl
https://istaustria.zoom.us/j/95779228618?pwd=N3huS0loYnNPcEFhd1BROUVIT0xIdz09

**Meeting ID:** 957 7922 8618
**Passcode:** 818622

***
### Lora Sweeney
https://istaustria.zoom.us/j/95325802080?pwd=YVlyQ2NqTW85YWNSdzdYSXV5bTJaQT09

**Meeting ID:** 953 2580 2080
**Passcode:** 540714

***
### Gaia Novarino
https://istaustria.zoom.us/j/95650646988?pwd=elZuNS9IbmtQVkNzaVZRTEl1WU5sdz09

**Meeting ID:** 956 5064 6988
**Passcode:** 686040

***
### Nick Barton
https://istaustria.zoom.us/j/97112291336?pwd=NlN1cjZlRlRmdk0wQS9FNHJ0a2YyUT09

**Meeting ID:** 971 1229 1336
**Passcode:** 870427

***
### Mario de Bono
https://istaustria.zoom.us/j/96271989448?pwd=bUlWcmlqNmtBV3NZdE1VTWxnNlpQZz09

**Meeting ID:** 962 7198 9448
**Passcode:** 890302

***
### Beatriz Vicoso
https://istaustria.zoom.us/j/91085461615?pwd=KzYwTFprUE8yclpBYmNkT3g2ZGM0QT09

**Meeting ID:** 910 8546 1615
**Passcode:** 564879

***
### Simon Hippenmeyer
https://istaustria.zoom.us/j/95546608988?pwd=NmFhRzk2V3pCWUZQaWV6RmM2MUhDdz09

**Meeting ID:** 955 4660 8988
**Passcode:** 325343